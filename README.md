# TUISHARE - open source voice chat

## License

All the files under this project are licensed under the [Mozilla Public License 2.0](https://www.mozilla.org/en-US/MPL/2.0/)

---

Copyright (c) Kunal Dandekar 2022
